import React from 'react';
import './App.css';
import Greeting from './Gret';
import Clock from './Clock'
import Avatar from './UserProfile/Avatar';
import UserName from './UserProfile/UserName';
import Bio from './UserProfile/Bio';
import UserProfile from './UserProfile/UserProfile';
import logo from './logo.svg';
import "bootstrap/dist/css/bootstrap.min.css";
import Hooks from './Nav/Hooks';


function App() {

  return (
    <div className="App">
      <header className="App-header">
      <h2>Soal no 2.2</h2>
      <img src={logo} alt="Smiley face" className="App-logo"></img>
      <Greeting name="budi" age="31" gender="Male"/>
      <br/>
      <p>Soal No 2.3</p>
      <UserProfile></UserProfile>
      <br/>
      <p>Soal no 2.3 dipisah</p>
      <Avatar></Avatar>
      <UserName username="Ariya Setiawan"></UserName>
      <Hooks user="Ariya Setiawan"></Hooks>
      <Bio></Bio>
      
      <p></p>
      <Clock />
      </header>


    </div>
  );
}

export default App;