import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import App from "./App";
import About from "./Nav/About";
import NotFound from "./Nav/notFound";
import Profile from "./Nav/Profile";
import Form from "./Nav/Form";
import Clock from "./Clock";
import Login from "./Nav/Login";
import Register from "./Nav/Register";
import Hooks from "./Nav/Hooks";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.min.css";
import logo from "./logo.svg";
import viewbook from "./Nav/Axios_ViewBooks";
import addbook from "./Nav/Axios_AddBooks";
import deletebook from "./Nav/Axios_DeleteBooks";
import editbook from "./Nav/Axios_EditBooks";
import formik from "./Nav/formik";
import "bootstrap/dist/js/bootstrap.min.js";

const routing = (
  <Router>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        <img src={logo} alt="Smiley face" className="App-logo"></img>
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link className="navbar-brand" to="/">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/profile">
              Profile
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/about">
              About
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/form">
              >>>>
            </Link>
          </li>

         
          <li className="nav-item">
            <Link className="navbar-brand" to="/axios/viewbook">
              ViewBooks
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/axios/addbook">
              AddBooks
            </Link>
          </li>
        </ul>
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="navbar-brand" to="/register">
              SignUp
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/login">
              Login
            </Link>
            </li>

          <li className="nav-item">
            <Link className="navbar-brand" to="/hooks">
              <Clock />
            </Link>
          </li>
        </ul>
      </div>
    </nav>
    <Switch>
              
      <Route exact path="/" component={App} />
              
      <Route path="/about/:param?" component={About} />
              
      <Route path="/profile/" component={Profile} />
      <Route path="/form/" component={Form} />
      <Route path="/register/" component={Register} />
      <Route path="/login/" component={Login} />
      <Route path="/hooks/" component={Hooks} />
      <Route path="/axios/viewbook" component={viewbook} />
      <Route path="/axios/addbook" component={addbook} />
      <Route path="/axios/deletebook/:param?" component={deletebook} />
      <Route path="/axios/editbook/:param?" component={editbook} />
      <Route path="/axios/formik" component={formik} />
              
      <Route component={NotFound} />
          
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));
// ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
