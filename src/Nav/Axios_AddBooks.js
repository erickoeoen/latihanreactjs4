import React from "react";
import Axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import "bootstrap/dist/css/bootstrap.min.css";

const AddBooks = () => (
  <div>
    <h2 align="center"> Add Books </h2>
    <Formik
      initialValues={{
        id: "",
        title: "",
        author: "",
        published_date: "",
        pages: "",
        language: "",
        publisher_id: ""
      }}
      validate={values => {
        const errors = {};
        if (!values.title) {
          errors.title = "Title Required";
        } else if (!values.author) {
          errors.author = "Author Required";
        } else if (!values.published_date) {
          errors.published_date = "Published Date Required";
        } else if (!values.pages) {
          errors.pages = "pages Required";
        } else if (!values.language) {
          errors.language = "language Required";
        } else if (!values.publisher_id) {
          errors.publisher_id = "published id Required";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          console.log(values.body);
          Axios.post("http://localhost:4000/books/", values)
            .then(response => {
              alert("response : " + response.data.message);
              window.location = "/axios/viewbook";
            })
            .catch(function(error) {
              console.log("cacth error" + error);
            });

          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <div className="container">
          <Form>
            <div className="form-group ">
              <div className="input-group-prepend"></div>
              <Field
                placeholder="id"
                type=""
                name="id"
                className="form-control"
                readOnly="true"
              />{" "}
              <br />
              <Field
                placeholder="title"
                type=""
                name="title"
                className="form-control"
              />
              <Field
                placeholder="author"
                type=""
                name="author"
                className="form-control"
              />
              <Field
                placeholder="published_date"
                type="date"
                name="published_date"
                className="form-control"
              />
              <br />
              <Field
                placeholder="pages"
                type="number"
                name="pages"
                className="form-control"
              />
              <br />
              <Field
                placeholder="language"
                type=""
                name="language"
                className="form-control"
              />
              <br />
              <Field
                placeholder="publisher_id"
                type=""
                name="publisher_id"
                className="form-control"
              />
              <br />
              <button
                type="submit"
                className="btn btn-primary"
                disabled={isSubmitting}
              >
                Submit
              </button>
              <ErrorMessage
                name="title"
                component="div"
                className="alert alert-danger"
                role="alert"
              />
              <ErrorMessage
                name="author"
                component="div"
                className="alert alert-danger"
                role="alert"
              />
              <ErrorMessage
                name="published_date"
                component="div"
                className="alert alert-danger"
                role="alert"
              />
              <ErrorMessage
                name="pages"
                component="div"
                className="alert alert-danger"
                role="alert"
              />
              <ErrorMessage
                name="language"
                component="div"
                className="alert alert-danger"
                role="alert"
              />
              <ErrorMessage
                name="publisher_id"
                component="div"
                className="alert alert-danger"
                role="alert"
              />
            </div>
          </Form>
        </div>
      )}
    </Formik>
  </div>
);

export default AddBooks;
