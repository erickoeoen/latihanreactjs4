import React from "react";
import Axios from "axios";

export default class PersonList extends React.Component {
  //constructor(props)
  state = {
    //   super(props);
    person: []
  };

  componentDidMount() {
    //this.timerID = setInterval(() => this.updateClock(), 1000);
    Axios.get("https://jsonplaceholder.typicode.com/users").then(res => {
      const person = res.data;
      this.setState({ person });
    });
  }

  // componentWillMount(){
  //     clearInterval(this.timerID)
  // }

  // updateClock()
  // {
  //     this.setState({
  //         date: new Date()
  //     })
  // }

  render() {
    return (
      <div>
        <table>
          <tr>
            <th>id</th>
            <th>name</th>
            <th>username</th>
          </tr>
          {this.state.person.map(persons => (
            <tr key={persons._id}>
              <td>{persons.id}</td>
              <td>{persons.name}</td>
              <td>{persons.username}</td>
            </tr>
          ))}
        </table>
        {/* {this.state.date.toLocaleTimeString()}         */}
      </div>
    );
  }
}
