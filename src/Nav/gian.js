import React from 'react'
import axios from 'axios'
import moment from 'moment'
import { Table, Col, Button, Row, Form, FormGroup, Label, Input } from "reactstrap";

export default class GetBook extends React.Component {

    state = {
        books: [],
        id: '',
        title: '',
        author: '',
        publishedDate: '',
        pages: '',
        language: '',
        publisherId: '',
        editMode: false,
    };

    componentDidMount = () => {
        axios.get('http://localhost:8002/books').then(res => {
            const books = res.data;
            this.setState({ books: books.data });
        });
    }

    handleChange = (event) => {
        const name = event.target.name
        const value = event.target.value

        this.setState({
            [name]: value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const data = {
            title: this.state.title,
            author: this.state.author,
            publishedDate: this.state.publishedDate,
            pages: this.state.pages,
            language: this.state.language,
            publisherId: this.state.publisherId
        }
        console.log(data)
        axios.post(`http://localhost:8002/books`, data)
            .then((res) => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            })
            .finally(() => {
                this.componentDidMount()
            })
    }

    handleEdit = bookId => {
        axios.get(`http://localhost:8002/books/${bookId}`)
            .then(result => {
                const temp = result.data.data.publishedDate
                const publishedDate = moment(temp).format('YYYY-MM-DD')
                this.setState({
                    id: result.data.data.id,
                    title: result.data.data.title,
                    author: result.data.data.author,
                    publishedDate: publishedDate,
                    pages: result.data.data.pages,
                    language: result.data.data.language,
                    publisherId: result.data.data.publisherId,
                    editMode: true
                })
                console.log(this.state.publishedDate)
            })
            .catch(err => {
                console.log(err)
            })
        console.log(this.state.editMode)
    }

    handleEditBook = (event) => {
        event.preventDefault()
        const id = this.state.id
        const data = {
            title: this.state.title,
            author: this.state.author,
            publishedDate: this.state.publishedDate,
            pages: this.state.pages,
            language: this.state.language,
            publisherId: this.state.publisherId
        }
        axios.put(`http://localhost:8002/books/${id}`, data)
            .then((res) => {
                if (res.status===200){
                    this.setState({
                        id: '',
                        title: '',
                        author: '',
                        publishedDate: '',
                        pages: '',
                        language: '',
                        publisherId: '',
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
            .finally(() => {
                this.componentDidMount()
                this.setState({
                    editMode: false
                })
            })
    }


    handleDelete = bookId => {
        axios.delete(`http://localhost:8002/books/${bookId}`)
            .then(result => {
                console.log(result)

            })
            .catch(err => {
                console.log(err)
            })
            .finally(() => {
                this.componentDidMount()
            })
    }


    render() {
        return (
            <div className='container'>
                <Row>
                    <Col>
                        <Form className="mt-5 mb-5" onSubmit={this.state.editMode ? this.handleEditBook : this.handleSubmit}>
                            <h2>Form Book</h2>
                            <FormGroup>
                                <Label for="title">Title</Label>
                                <Input type="text" name="title" id="Title" value={this.state.editMode ? this.state.title : this.state.title} placeholder="Input the title of book" onChange={this.handleChange} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="author">Author</Label>
                                <Input type="text" name="author" id="author" value={this.state.editMode ? this.state.author : this.state.author} placeholder="Input the author of book" onChange={this.handleChange} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="publishedDate">Date Published</Label>
                                <Input type="date" name="publishedDate" id="publishedDate" value={this.state.editMode ? this.state.publishedDate : this.state.publishedDate} placeholder="Choose the date" onChange={this.handleChange} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="pages">Pages</Label>
                                <Input type="number" name="pages" id="pages" value={this.state.editMode ? this.state.pages : this.state.pages} placeholder="Input the pages of date" onChange={this.handleChange} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="language">Language</Label>
                                <Input type="text" name="language" id="language" value={this.state.editMode ? this.state.language : this.state.language} placeholder="Input the language of book" onChange={this.handleChange} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="publisherId">Publisher Name</Label>
                                <Input type="text" name="publisherId" id="publisherId" value={this.state.editMode ? this.state.publisherId : this.state.publisherId} placeholder="Input the publisher name" onChange={this.handleChange} />
                            </FormGroup>
                            <Button type="submit">{this.state.editMode?'Edit':'Submit'}</Button>
                        </Form>
                    </Col>
                    <Col>
                        <Table className="mt-5 ml-3">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Date Published</th>
                                    <th>Pages</th>
                                    <th>Language</th>
                                    <th>Publisher Name</th>
                                    <th colSpan="3" className="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.books.map(book => (
                                    <tr key={book.id}>
                                        <td>{book.title}</td>
                                        <td>{book.author}</td>
                                        <td>{book.publishedDate}</td>
                                        <td>{book.pages}</td>
                                        <td>{book.language}</td>
                                        <td>{book.publisherId}</td>
                                        <td><Button onClick={() => { this.handleEdit(book.id) }}>Edit</Button></td>
                                        <td><Button onClick={() => { this.handleDelete(book.id) }}>Delete</Button></td>
                                        <td><Button onClick={this.toggle}>Detail</Button></td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}

